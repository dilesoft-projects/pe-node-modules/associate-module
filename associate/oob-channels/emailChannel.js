const { query } = require('nact');
const nodemailer = require('nodemailer');

export default async function (msg, ctx) {
  const { client_id } = msg;
  const { user_id } = msg;
  const email = msg.associate_hint;
  const { user_code } = msg;

  // if(!user.email_verified){}

  const collectionItemActor = ctx.children.get('item');

  // create reusable transporter object using the default SMTP transport
  const transporter = nodemailer.createTransport('smtps://user%40gmail.com:pass@smtp.gmail.com');

  // setup e-mail data with unicode symbols
  const mailOptions = {
    from: '"Protopia" <protopia@gmail.com>', // sender address
    to: email, // list of receivers
    subject: 'One-Time Password', // Subject line
    text: `password: ${user_code}`, // plaintext body
    html: `<b>password: ${user_code}</b>`, // html body
  };

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log(`Message sent: ${info.response}`);
  });

  await query(collectionItemActor, {
    type: 'associate_session',
    input: {
      email,
      user_code,
    },
  }, global.actor_timeout);
}
